/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package databaseprogram;
import java.sql.*;
import java.util.Scanner;

class DB {
    public DB() {} // Constructor for the DB class

    // Method for connecting to a database
    public Connection dbConnect(String db_connect_string, String db_userid, String db_password) {
        Connection con = null;
        try {
            // Load the SQL Server driver and establish a database connection
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = DriverManager.getConnection(db_connect_string, db_userid, db_password);
            con = conn;
        }
        catch (ClassNotFoundException | SQLException e) {
            System.out.println("Something gone wrong. Try again");
            //e.printStackTrace();
        }
        return con;
    }

    // Method for inserting data into a table
    public void insert(Connection conn) {
        try {
            // Read table name and column information, then insert data into the specified table
        }
        catch (SQLException e) {
            //e.printStackTrace();
        }
    }

    // Method for handling user interactions based on their role (AdminStaff, Student, Teacher)
    public void users(Connection conn) throws SQLException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter login: ");
        String user = sc.nextLine();
        switch (user) {
            case "AdminStaff" -> {
                // Handle actions for AdminStaff users
            }
            case "Student" -> {
                // Handle actions for Student users
            }
            case "Teacher" -> {
                // Handle actions for Teacher users
            }
            default -> {
                System.out.println("There is no such user.");
                System.exit(0);
            }
        }
    }

    // Methods for modifying contact details, grades, teacher assignments, and displaying subjects
    public void contact_details(Connection conn) throws SQLException {
        // Update contact details for a student
    }

    public void grades(Connection conn) throws SQLException {
        // Modify or add grades for a student
    }

    public void group(Connection conn) throws SQLException {
        // Modify teacher assignments for a group of classes
    }

    public void subject(Connection conn) throws SQLException {
        // Display a list of subjects with semester numbers
    }

    // Method for updating data in a table
    public void update(Connection conn) {
        try {
            // Update data in a table based on user input
        }
        catch (SQLException e) {
            //e.printStackTrace();
            System.out.println("You cannot do that");
        }
    }

    // Method for deleting data from a table
    public void delete(Connection conn) {
        try {
            // Delete data from a table based on user input
        }
        catch (SQLException e) {
            //e.printStackTrace();
            System.out.println("You cannot do that");
        }
    }
};

public class DatabaseProgram {
    public static void main(String[] args) throws SQLException {
        DB db = new DB();
        System.out.println("Welcome to the database program!!!");
        
        Connection conn = db.dbConnect("jdbc:sqlserver://localhost:1433;databaseNLame=Lab2;selectMethod=cursor","AdminStaff", "Admin1234");
        db.users(conn);
        conn.close();
    }
}
